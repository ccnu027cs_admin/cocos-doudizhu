<<<<<<< HEAD
# Cocos Creator 4 斗地主

## 概述
使用creator开发的斗地主， 使用socket通讯，实现了斗地主基本的功能，方便入门的同学参考。

## 开发环境
1. Cocos Creator V1.3.0
2. Nodejs
3. socket.io

## 环境搭建步骤
1. 安装nodejs，以及express，socket.io模块
2. cd到server.js所在目录(将server目录下的三个js文件copy到express项目下面)， 运行： `node server.js`
3. 打开creator，打开本实例工程，运行即可。

## 已实现功能
1. 创建房间跟加入房间
2. 服务端洗牌发牌
3. 点击选牌，拖拉选牌，双击取消所选牌
4. 牌型校验

## 游戏截图
 ![image](https://github.com/dixonzhang/cocos-doudizhu/blob/master/show.png)
=======
# cocos-doudizhu


#### 介绍

使用creator开发的斗地主， 使用socket通讯，实现了斗地主基本的功能，方便入门的同学参考。


Cocos Creator 4 斗地主

概述

使用creator开发的斗地主， 使用socket通讯，实现了斗地主基本的功能，方便入门的同学参考。


开发环境

Cocos Creator V1.3.0

Nodejs

socket.io

环境搭建步骤

安装nodejs，以及express，socket.io模块

cd到server.js所在目录(将server目录下的三个js文件copy到express项目下面)， 运行： node server.js

打开creator，打开本实例工程，运行即可。

已实现功能

创建房间跟加入房间

服务端洗牌发牌

点击选牌，拖拉选牌，双击取消所选牌

牌型校验

游戏截图

 ![image](https://github.com/dixonzhang/cocos-doudizhu/blob/master/show.png)


>>>>>>> 2bbc214b69c44aeed254fc5a8a72270722993894
